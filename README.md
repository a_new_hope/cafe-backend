# Установка проекта #

Установка необходимых пакетов

```
#!bash

sudo apt-get install python3 libpq-dev python-dev postgresql postgresql-contrib
```

Настройка базы данных

```
#!bash

sudo su - postgres
createdb cafe_dev
psql
GRANT ALL PRIVILEGES ON DATABASE cafe_dev TO postgres;

```
Потом в /etc/postgresql/9.4/main/pg_hba.conf 
изменяем с

```
#!bash

host    all             all             127.0.0.1/32            md5
```

на

```
#!bash

host    all             all             127.0.0.1/32            trust
```
Перезапускаем postgresql
```
#!bash

sudo service postgresql restart
```


Разворачивание проекта
```
#!bash

wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install virtualenv
git clone git@bitbucket.org:a_new_hope/cafe-backend.git
cd cafe-backend
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```
После чего наше приложение доступно по адресу http://127.0.0.1:8000/

Админка - http://127.0.0.1:8000/admin/

Создание дампа бд

```
#!bash
sudo su postgres
pg_dump cafe_dev -f /tmp/cafe_dev.sql
```

Импортирование дампа бд

```
#!bash
sudo su postgres
psql
DROP DATABASE cafe_dev;
CREATE DATABASE cafe_dev;
\q
psql cafe_dev < /tmp/cafe_dev.sql
```

Admin user login: admin, pass: 1

Manager user login: manager, pass: 1

Worker user login: worker, pass: 1