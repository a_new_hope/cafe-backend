from django.contrib import admin
from accounts.models import StuffUser

class StuffUserAdmin(admin.ModelAdmin):
    list_display = ['user', 'gender',]
    raw_id_fields = ['user']

admin.site.register(StuffUser, StuffUserAdmin)