from django import forms
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthForm

class AjaxFormMixin(object):

    def get_error(self):
        error_item = self.errors.items()[0]
        if error_item[0] == '__all__':
            error = error_item[1][0]
        else:
            error = "%s: %s" % (error_item[0], error_item[1][0])
        return error


class AuthenticationForm(DjangoAuthForm, AjaxFormMixin):

    error_messages = {
        'invalid_login': _("Please enter a correct %(username)s and password."),
        'inactive': _("This account is inactive."),
    }

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            elif not self.user_cache.is_active:
                raise forms.ValidationError(
                    self.error_messages['inactive'],
                    code='inactive',
                )
        return self.cleaned_data