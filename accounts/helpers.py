from accounts.models import StuffUser
import json
from django.http import HttpResponse


def json_response(response_dict, status=200):
    response = HttpResponse(json.dumps(response_dict), content_type="application/json", status=status)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response

def get_user_data(user):
    data = {
        'user_id': user.id,
        'username': user.username,
        'email': user.email,
        'role': user.stuffuser.role,
    }
    return data