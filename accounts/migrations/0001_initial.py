# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StuffUser',
            fields=[
                ('user', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('role', models.IntegerField(default=3, max_length=1, choices=[(1, b'Administrator'), (2, b'Manager'), (3, b'Worker')])),
                ('gender', models.IntegerField(default=1, max_length=1, choices=[(1, b'Male'), (2, b'Female')])),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('phone_number', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
