# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stuffuser',
            name='gender',
            field=models.IntegerField(default=1, max_length=1, choices=[(0, b'Male'), (1, b'Female')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='stuffuser',
            name='role',
            field=models.IntegerField(default=3, max_length=1, choices=[(0, b'Administrator'), (1, b'Manager'), (2, b'Worker')]),
            preserve_default=True,
        ),
    ]
