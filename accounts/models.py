from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, date
import binascii
import os

GENDER_CHOICES = (
    (0, 'Male'),
    (1, 'Female'),
)

USER_ROLES = (
    (0, 'Administrator'),
    (1, 'Manager'),
    (2, 'Worker'),
)

class StuffUser(models.Model):
    user = models.OneToOneField(User, unique=True, primary_key=True)
    role = models.IntegerField(max_length=1, choices=USER_ROLES, default=3)
    gender = models.IntegerField(max_length=1, choices=GENDER_CHOICES, default=1)
    date_of_birth = models.DateField(blank=True, null=True)
    phone_number = models.IntegerField(default=0)