from accounts.models import StuffUser
from rest_framework import serializers
from django.contrib.auth.models import User


class StuffUserSerializer(serializers.ModelSerializer):
    # user = serializers.StringRelatedField()
    # email = serializers.StringRelatedField(source='user.email')

    class Meta:
        model = StuffUser
        fields = ('gender', 'role')


class UserSerializer(serializers.ModelSerializer):
    stuffuser = StuffUserSerializer()

    class Meta:
        model = User
        fields = ('id', 'password', 'first_name', 'last_name', 'email', 'username', 'last_login', 'date_joined', 'stuffuser')
        extra_kwargs = {
            'password': {'write_only': True, 'required': False}
        }

    def create(self, validated_data):
        stuff_user_data = validated_data.pop('stuffuser')
        user = User.objects.create(**validated_data)
        password = validated_data.pop('password')
        user.set_password(password)
        user.save()
        StuffUser.objects.create(user=user, **stuff_user_data)
        return user

    def update(self, instance, validated_data):
        stuff_user_data = validated_data.pop('stuffuser')
        stuff_user = instance.stuffuser

        password = validated_data.get('password', instance.password)

        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        if len(password) < 15:  # weird hack. need to refactor this code.
            instance.set_password(password)
        instance.save()

        stuff_user.gender = stuff_user_data.get('gender', stuff_user.gender)
        stuff_user.role = stuff_user_data.get('role', stuff_user.role)
        stuff_user.save()
        return instance