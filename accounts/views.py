from django.contrib.auth.models import User
from django.db import IntegrityError
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from accounts.helpers import json_response, get_user_data

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from accounts.models import StuffUser
from accounts.serializers import StuffUserSerializer, UserSerializer
from rest_framework import permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework import generics
from rest_framework.views import APIView


@csrf_exempt
def register(request):
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)

        if username is not None and password is not None:
            try:
                user = User.objects.create_user(username, None, password)
            except IntegrityError:
                return json_response({
                    'error': 'User already exists'
                }, status=400)
            token = Token.objects.create(user=user)
            print 'success'
            return json_response({
                'token': token.token,
                'username': user.username,
                'user': get_user_data(user)
            })
        else:
            return json_response({
                'error': 'Invalid Data'
            }, status=400)
    elif request.method == 'OPTIONS':
        return json_response({})
    else:
        return json_response({
            'error': 'Invalid Method'
        }, status=405)


class LoginView(ObtainAuthToken):
    def post(self, request):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'user': get_user_data(user)})

@csrf_exempt
def logout(request):
    if request.method == 'POST':
        request.token.delete()
        return json_response({
            'status': 'success'
        })
    elif request.method == 'OPTIONS':
        return json_response({})
    else:
        return json_response({
            'error': 'Invalid Method'
        }, status=405)


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class StuffUserList(generics.ListCreateAPIView):
    queryset = StuffUser.objects.all()
    serializer_class = StuffUserSerializer

class StuffUserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StuffUser.objects.all()
    serializer_class = StuffUserSerializer
