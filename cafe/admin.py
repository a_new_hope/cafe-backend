from django.contrib import admin
from cafe.models import Table, Product, Order, ProductsOrder, CategoryMenu

class TableAdmin(admin.ModelAdmin):
    list_display = ['name', 'status']

admin.site.register(Table, TableAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'price', 'created_date']

admin.site.register(Product, ProductAdmin)

class ProductsOrderInline(admin.TabularInline):
    model = ProductsOrder
    extra = 2 # how many rows to show

class OrderAdmin(admin.ModelAdmin):
    inlines = (ProductsOrderInline,)
    list_display = ['table', 'user', 'status', 'created_date', 'updated_date']

admin.site.register(Order, OrderAdmin)

class CategoryMenuAdmin(admin.ModelAdmin):
    list_display = ['name', 'parent']

admin.site.register(CategoryMenu, CategoryMenuAdmin)