# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cafe.models
import datetime
from decimal import Decimal
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryMenu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('image', models.ImageField(null=True, upload_to=cafe.models.generate_category_menu_image, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('parent', models.ForeignKey(blank=True, to='cafe.CategoryMenu', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.IntegerField(default=0, max_length=1, choices=[(0, b'Adopted'), (1, b'Waiting'), (2, b'Rejected'), (3, b'Closed')])),
                ('amount', models.DecimalField(default=Decimal('0.00'), max_digits=7, decimal_places=2)),
                ('created_date', models.DateTimeField(default=datetime.datetime.now)),
                ('updated_date', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('price', models.DecimalField(default=Decimal('0.00'), max_digits=7, decimal_places=2)),
                ('created_date', models.DateTimeField(default=datetime.datetime.now)),
                ('category', models.ForeignKey(to='cafe.CategoryMenu')),
            ],
        ),
        migrations.CreateModel(
            name='ProductsOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.PositiveIntegerField(default=1)),
                ('order', models.ForeignKey(to='cafe.Order')),
                ('product', models.ForeignKey(to='cafe.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Table',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('status', models.IntegerField(default=0, max_length=1, choices=[(0, b'Free'), (1, b'Busy'), (2, b'Reserved')])),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='products',
            field=models.ManyToManyField(to='cafe.Product', through='cafe.ProductsOrder'),
        ),
        migrations.AddField(
            model_name='order',
            name='table',
            field=models.ForeignKey(to='cafe.Table'),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
