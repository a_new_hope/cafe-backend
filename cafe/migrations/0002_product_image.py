# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cafe.models


class Migration(migrations.Migration):

    dependencies = [
        ('cafe', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='image',
            field=models.ImageField(null=True, upload_to=cafe.models.generate_product_image, blank=True),
        ),
    ]
