# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cafe', '0003_product_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productsorder',
            name='order',
            field=models.ForeignKey(blank=True, to='cafe.Order', null=True),
        ),
    ]
