from django.db import models
from decimal import Decimal
from datetime import datetime
from django.contrib.auth.models import User
import uuid


class Table(models.Model):
    TABLE_CHOICES = (
        (0, 'Free'),
        (1, 'Busy'),
        (2, 'Reserved'),
    )

    name = models.CharField(max_length=250)
    status = models.IntegerField(max_length=1, choices=TABLE_CHOICES, default=0)

    def __unicode__(self):
        return self.name


def generate_product_image(self, filename):
    ext = filename.split('.')[-1]
    return 'product/{0}.{1}'.format(uuid.uuid4(), ext)


class Product(models.Model):

    name = models.CharField(max_length=250)
    category = models.ForeignKey('CategoryMenu')
    image = models.ImageField(upload_to=generate_product_image, blank=True, null=True)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=Decimal('0.00'))
    created_date = models.DateTimeField(default=datetime.now)
    description = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.name


class Order(models.Model):
    ORDER_CHOICES = (
        (0, 'Adopted'),
        (1, 'Waiting'),
        (2, 'Rejected'),
        (3, 'Closed'),
    )

    table = models.ForeignKey(Table)
    user = models.ForeignKey(User)
    status = models.IntegerField(max_length=1, choices=ORDER_CHOICES, default=0)
    products = models.ManyToManyField(Product, through='ProductsOrder')
    amount = models.DecimalField(max_digits=7, decimal_places=2, default=Decimal('0.00'))
    created_date = models.DateTimeField(default=datetime.now)
    updated_date = models.DateTimeField(auto_now=True)

    # def __str__(self):
    # return str(self.table.name)


class ProductsOrder(models.Model):
    product = models.ForeignKey(Product)
    order = models.ForeignKey(Order, blank=True, null=True)
    quantity = models.PositiveIntegerField(default=1)


def generate_category_menu_image(self, filename):
    ext = filename.split('.')[-1]
    return 'category-menu/{0}.{1}'.format(uuid.uuid4(), ext)


class CategoryMenu(models.Model):
    parent = models.ForeignKey('self', blank=True, null=True)
    name = models.CharField(max_length=250)
    image = models.ImageField(upload_to=generate_category_menu_image, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.name
