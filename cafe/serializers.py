from cafe.models import Table, Product, Order, ProductsOrder, CategoryMenu
from rest_framework import serializers


class CategoryMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryMenu


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table


class ProductSerializer(serializers.ModelSerializer):
    # duplicate id as product variable. because Products are being used as manytomany for Orders
    product = serializers.CharField(source='id', read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'product', 'name', 'category', 'image', 'price', 'description')


class ProductsOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductsOrder


class OrderSerializer(serializers.ModelSerializer):
    products = ProductsOrderSerializer(source='productsorder_set', many=True)

    class Meta:
        model = Order
        fields = ('table', 'user', 'status', 'products', 'amount', 'created_date', 'updated_date')
        extra_kwargs = {
            'user': {'required': False}
        }

    def validate_products(self, value):
        """
        Check if products array did`t not empty.
        """
        if not value:
            raise serializers.ValidationError("You can`t save empty order")
        return value

    def create(self, validated_data):
        user = self.context['request'].user
        table = validated_data.get('table')
        amount = validated_data.get('amount')
        order = Order.objects.create(table=table, user=user, amount=amount)

        products_data = validated_data.pop('productsorder_set')
        for product in products_data:
            ProductsOrder.objects.create(product=product.get('product'), order=order,
                                         quantity=product.get('quantity'))
        return order

    def update(self, instance, validated_data):
        instance.user = self.context['request'].user
        instance.table = validated_data.get('table', instance.table)
        instance.status = validated_data.get('status', instance.status)

        products_data = validated_data.pop('productsorder_set')
        if products_data:
            instance.products.clear()
            for product in products_data:
                ProductsOrder.objects.create(product=product.get('product'), order=instance,
                                             quantity=product.get('quantity'))
        instance.save()
        return instance
