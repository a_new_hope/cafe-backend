from rest_framework import generics
from cafe.serializers import TableSerializer, ProductSerializer, OrderSerializer, CategoryMenuSerializer
from cafe.models import Table, Product, Order, CategoryMenu


class TableList(generics.ListCreateAPIView):
    queryset = Table.objects.all()
    serializer_class = TableSerializer

    def get_queryset(self):
        queryset = Table.objects.all()
        status = self.request.QUERY_PARAMS.get('status', None)
        if status:
            queryset = queryset.filter(status=int(status))
        return queryset


class TableDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class ProductList(generics.ListCreateAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        queryset = Product.objects.all()
        category = self.request.QUERY_PARAMS.get('category', None)
        if category:
            queryset = queryset.filter(category__id=int(category))
        return queryset


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class OrdertList(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class OrderDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class CategoryMenutList(generics.ListCreateAPIView):
    serializer_class = CategoryMenuSerializer

    def get_queryset(self):
        queryset = CategoryMenu.objects.all()
        parent = self.request.QUERY_PARAMS.get('parent', None)
        if parent == 'false':
            queryset = queryset.filter(parent=None)
        if parent.isdigit():
            queryset = queryset.filter(parent=parent)
        return queryset


class CategoryMenuDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CategoryMenu.objects.all()
    serializer_class = CategoryMenuSerializer
