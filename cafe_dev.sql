--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts_stuffuser; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accounts_stuffuser (
    user_id integer NOT NULL,
    role integer NOT NULL,
    gender integer NOT NULL,
    date_of_birth date,
    phone_number integer NOT NULL
);


ALTER TABLE public.accounts_stuffuser OWNER TO postgres;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO postgres;

--
-- Name: cafe_categorymenu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_categorymenu (
    id integer NOT NULL,
    name character varying(250) NOT NULL,
    image character varying(100),
    description text,
    parent_id integer
);


ALTER TABLE public.cafe_categorymenu OWNER TO postgres;

--
-- Name: cafe_categorymenu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_categorymenu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_categorymenu_id_seq OWNER TO postgres;

--
-- Name: cafe_categorymenu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_categorymenu_id_seq OWNED BY cafe_categorymenu.id;


--
-- Name: cafe_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_order (
    id integer NOT NULL,
    status integer NOT NULL,
    amount numeric(7,2) NOT NULL,
    created_date timestamp with time zone NOT NULL,
    updated_date timestamp with time zone NOT NULL,
    table_id integer NOT NULL,
    user_id integer
);


ALTER TABLE public.cafe_order OWNER TO postgres;

--
-- Name: cafe_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_order_id_seq OWNER TO postgres;

--
-- Name: cafe_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_order_id_seq OWNED BY cafe_order.id;


--
-- Name: cafe_product; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_product (
    id integer NOT NULL,
    name character varying(250) NOT NULL,
    price numeric(7,2) NOT NULL,
    created_date timestamp with time zone NOT NULL,
    category_id integer NOT NULL,
    image character varying(100),
    description text
);


ALTER TABLE public.cafe_product OWNER TO postgres;

--
-- Name: cafe_product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_product_id_seq OWNER TO postgres;

--
-- Name: cafe_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_product_id_seq OWNED BY cafe_product.id;


--
-- Name: cafe_productsorder; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_productsorder (
    id integer NOT NULL,
    quantity integer NOT NULL,
    order_id integer,
    product_id integer NOT NULL,
    CONSTRAINT cafe_productsorder_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.cafe_productsorder OWNER TO postgres;

--
-- Name: cafe_productsorder_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_productsorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_productsorder_id_seq OWNER TO postgres;

--
-- Name: cafe_productsorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_productsorder_id_seq OWNED BY cafe_productsorder.id;


--
-- Name: cafe_table; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_table (
    id integer NOT NULL,
    name character varying(250) NOT NULL,
    status integer NOT NULL
);


ALTER TABLE public.cafe_table OWNER TO postgres;

--
-- Name: cafe_table_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_table_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_table_id_seq OWNER TO postgres;

--
-- Name: cafe_table_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_table_id_seq OWNED BY cafe_table.id;


--
-- Name: corsheaders_corsmodel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE corsheaders_corsmodel (
    id integer NOT NULL,
    cors character varying(255) NOT NULL
);


ALTER TABLE public.corsheaders_corsmodel OWNER TO postgres;

--
-- Name: corsheaders_corsmodel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE corsheaders_corsmodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.corsheaders_corsmodel_id_seq OWNER TO postgres;

--
-- Name: corsheaders_corsmodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE corsheaders_corsmodel_id_seq OWNED BY corsheaders_corsmodel.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_categorymenu ALTER COLUMN id SET DEFAULT nextval('cafe_categorymenu_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_order ALTER COLUMN id SET DEFAULT nextval('cafe_order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_product ALTER COLUMN id SET DEFAULT nextval('cafe_product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_productsorder ALTER COLUMN id SET DEFAULT nextval('cafe_productsorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_table ALTER COLUMN id SET DEFAULT nextval('cafe_table_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY corsheaders_corsmodel ALTER COLUMN id SET DEFAULT nextval('corsheaders_corsmodel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Data for Name: accounts_stuffuser; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY accounts_stuffuser (user_id, role, gender, date_of_birth, phone_number) FROM stdin;
1	0	1	2015-03-25	0
4	0	0	\N	0
5	0	0	\N	0
6	1	1	\N	0
7	2	0	\N	0
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add stuff user	7	add_stuffuser
20	Can change stuff user	7	change_stuffuser
21	Can delete stuff user	7	delete_stuffuser
22	Can add category menu	8	add_categorymenu
23	Can change category menu	8	change_categorymenu
24	Can delete category menu	8	delete_categorymenu
25	Can add token	9	add_token
26	Can change token	9	change_token
27	Can delete token	9	delete_token
28	Can add cors model	10	add_corsmodel
29	Can change cors model	10	change_corsmodel
30	Can delete cors model	10	delete_corsmodel
31	Can add table	11	add_table
32	Can change table	11	change_table
33	Can delete table	11	delete_table
34	Can add product	12	add_product
35	Can change product	12	change_product
36	Can delete product	12	delete_product
37	Can add order	13	add_order
38	Can change order	13	change_order
39	Can delete order	13	delete_order
40	Can add products order	14	add_productsorder
41	Can change products order	14	change_productsorder
42	Can delete products order	14	delete_productsorder
43	Can add category menu	15	add_categorymenu
44	Can change category menu	15	change_categorymenu
45	Can delete category menu	15	delete_categorymenu
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 45, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
4	pbkdf2_sha256$20000$Mk3upLNTSVHz$YI6LnZFIO4952fkSo6pL2q0KLenC6JSZU8KVo1Wsw6g=	\N	t	sauron07	Александр	Матвеев	matyeyev.sasha@gmail.com	t	t	2015-04-03 12:51:11+03
6	pbkdf2_sha256$20000$nnIj3e1q0DQB$DUxNZjncfMrII/r26FOiRKEDpWJE51xTNfw2y3oWVm4=	\N	f	manager	Юлия	Стадник		f	t	2015-04-03 13:42:45+03
5	pbkdf2_sha256$20000$QUc3XaImTy90$DF5LQVTDifVt5t+wFz4fNxn0UqF3IbFltZh627mn6dY=	\N	t	admin				t	t	2015-04-03 13:42:04+03
7	pbkdf2_sha256$20000$nnmRjnArvZOd$OrhiKZKudA06LxxLv8POzprKEvko/HImob+8nPdFh6g=	\N	f	worker	Максим	Кидрук		f	t	2015-04-03 13:44:19+03
1	pbkdf2_sha256$20000$NfHMOgYjXfEH$+yD9mt6xP8AfeHv3uyrTIe4jbg9W5OTv8h7OIm9VThs=	2015-04-07 14:41:20.051818+03	t	voffka	Владимир	Калюжный	voffka1990@gmail.com	t	t	2015-03-25 10:58:33.80349+02
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 7, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY authtoken_token (key, created, user_id) FROM stdin;
f4c10c343aee9114c43c38bd6bbb2fcf4dfc2ad1	2015-03-25 11:07:45.44781+02	1
\.


--
-- Data for Name: cafe_categorymenu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_categorymenu (id, name, image, description, parent_id) FROM stdin;
1	Кухня			\N
2	Блюда для компании	category-menu/3a3fb716-6778-42b4-9d9f-3edf31cd28b2.jpg		1
3	Блюда с живого огня	category-menu/19fd2337-ce8b-4ae2-afdd-86a89891ca40.jpg		1
4	Вареники	category-menu/9b61325d-43cf-4e63-958c-ebebd4b5432a.jpg		1
5	Гарниры	category-menu/6b1542e6-c6c8-4772-acdd-15abd9121053.jpg		1
6	Бар	category-menu/79d1cc38-dff6-415b-ae8b-41d363d5959d.jpg		\N
7	Бренди	category-menu/75e0fa49-0681-40d6-876b-6b269229491e.jpg		6
8	Вина Грузии	category-menu/0f6d8499-cd18-4ee3-89c8-77b5f4c3a55e.jpg		6
9	Вина Инкерман	category-menu/7511f90c-d566-4e5b-87ed-a1d4df44cc0e.jpg		6
10	Вина Франции	category-menu/733949de-6af5-4c86-ae14-283503829fea.jpg		6
11	Суши меню	category-menu/e2bcca7b-cf34-441d-9eb6-491c3d5481fd.jpg		\N
12	Горячие блюда	category-menu/448bed96-a800-495b-af3f-d6d147bed2b0.jpg		11
13	Горячие роллы	category-menu/eee410cd-8a9a-41e7-a290-d2d0bf6c147f.jpg		11
14	Детской меню	category-menu/758a5d76-f285-4b91-ac88-918114ce237e.jpg		\N
15	Горячие блюда			14
16	Канапе	category-menu/cf0f5edf-55c5-4c97-9dac-483fadc9bb71.jpg		14
17	Напитки			14
\.


--
-- Name: cafe_categorymenu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_categorymenu_id_seq', 17, true);


--
-- Data for Name: cafe_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_order (id, status, amount, created_date, updated_date, table_id, user_id) FROM stdin;
1	3	10.00	2015-04-07 17:02:14.283008+03	2015-04-07 17:02:14.296828+03	3	1
5	0	10.00	2015-04-07 17:20:11.885775+03	2015-04-07 17:20:11.895889+03	4	1
6	0	10.00	2015-04-07 17:20:41.714897+03	2015-04-07 17:20:41.715741+03	3	1
\.


--
-- Name: cafe_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_order_id_seq', 6, true);


--
-- Data for Name: cafe_product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_product (id, name, price, created_date, category_id, image, description) FROM stdin;
1	Сытная закуска	236.00	2015-04-03 13:48:25+03	2		буженина, бастурма, тосты с сырным салатом, ветчина, колбаса сыро-копченая, огурцы и помидоры соленые, рулет куриный, сало соленое, телятина шпигованная черносливом, язык говяжий, соус хрен и домашний майонез
2	Ассорти Япония	425.00	2015-04-03 13:49:21+03	2		Ролл "Санси"- 1 шт, ролл "Золотой дракон" - 1 шт, ролл "Сяке чиз" - 1 шт, суши "Лосось" - 2 шт, суши "Хияши вакаме" - 2 шт, сашими "Лосось" - 1 шт, сашими "Тгровая креветка", сашими "Омлет" - 1 шт
3	Ассорти из мяса на гриле	298.00	2015-04-03 13:49:48+03	2		Свинина, телятина, куриное филе, свиные ребрышки, овощи-гриль
4	Ассорти из рыбы и морепродуктов	427.00	2015-04-03 13:50:19+03	2		Семга, судак, тигровые креветки, мидии гигантские
5	Бараньи ребра	39.00	2015-04-03 13:50:40+03	3		
6	Кефаль филе	65.00	2015-04-03 13:51:01+03	3		
7	Колбаски ассорти	123.00	2015-04-03 13:51:14+03	3		
8	Куриное филе в беконе	43.00	2015-04-03 13:51:26+03	3		
9	Куриные крылышки	19.00	2015-04-03 13:51:38+03	3		
10	Вареники с капустой	22.00	2015-04-03 13:52:03+03	4		
11	Вареники с картофелем и грибами	22.00	2015-04-03 13:52:12+03	4		
12	Вареники с мясом	42.00	2015-04-03 13:52:24+03	4		
13	Драники из картофеля со сметаной	28.00	2015-04-03 14:47:34+03	5		
14	Картофель жареный по-домашнему на сале	25.00	2015-04-03 14:47:52+03	5		
15	Картофель жареный с грибами	25.00	2015-04-03 14:48:07+03	5		
16	Картофель запеченный в кожуре с травами	22.00	2015-04-03 14:48:20+03	5		
17	Ахтамар	57.00	2015-04-03 14:48:51+03	7		
18	Старый Кахети 3 зв.	28.00	2015-04-03 14:49:17+03	7		
19	Старый Кахети 5 зв.	32.00	2015-04-03 14:49:28+03	7		
20	Старый Кахети 7 зв.	35.00	2015-04-03 14:49:41+03	7		
21	Алазанская долина бел. п/сл.	285.00	2015-04-03 15:25:13+03	8		
22	Алазанская долина кр. п/сл.	285.00	2015-04-03 15:25:28+03	8		
23	Мукузани кр. сух.	320.00	2015-04-03 15:25:51+03	8		
24	Инкерман бел. п/сухое	180.00	2015-04-03 15:26:04+03	9		
25	Инкерман кр. п/сухое	180.00	2015-04-03 15:26:21+03	9		
26	Саблете Сотерн бел. сладкое	640.00	2015-04-03 15:26:33+03	10		
27	Лапша Удон	96.00	2015-04-03 15:26:59+03	12		с креветкой
28	Лапша Удон	54.00	2015-04-03 15:27:32+03	12		с курицей
29	Темпура кальмары	67.00	2015-04-03 15:27:52+03	12		
30	Хибачи рис	97.00	2015-04-03 15:28:09+03	12		с креветкой
31	Мега креветка	92.00	2015-04-03 15:28:28+03	13		с тигровыми креветками в темпуре, сыром, тобико и салатом "Айсберг"
32	Осака	112.00	2015-04-03 15:28:48+03	13		горячий ролл в темпурном кляре с лососем, гребешком и шитаке
33	Темпура-ролл	123.00	2015-04-03 15:29:05+03	13		горячий ролл в темпурном кляре с угрем и тигровой креветкой
34	Куриная котлета "Курочка ряба"	21.00	2015-04-03 15:29:23+03	15		
35	Куриные кораблики	27.00	2015-04-03 15:29:54+03	15		
36	Куриные нагетсы с кетчупом	29.00	2015-04-03 15:30:07+03	15		
37	Канапе с бужениной	7.00	2015-04-03 15:31:44+03	16		
38	Канапе с ветчиной	7.00	2015-04-03 15:31:59+03	16		
39	Канапе с копченой куриной грудкой	8.00	2015-04-03 15:32:14+03	16		
40	Канапе с семгой	14.00	2015-04-03 15:32:24+03	16		
41	Клубничный коктейль	38.00	2015-04-03 15:32:37+03	17		
42	Коктейль ананасово-молочный	32.00	2015-04-03 15:32:54+03	17		
43	 Коктейль шоколадно-банановый	45.00	2015-04-03 15:33:04+03	17		
44	Молочный коктейль "Тропик"	35.00	2015-04-03 15:33:13+03	17		
45	Соки в ассортименте	9.60	2015-04-03 15:33:23+03	17		
\.


--
-- Name: cafe_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_product_id_seq', 45, true);


--
-- Data for Name: cafe_productsorder; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_productsorder (id, quantity, order_id, product_id) FROM stdin;
1	2	1	1
2	4	1	5
3	1	5	1
4	1	5	2
5	1	5	3
6	1	6	22
7	1	6	21
8	1	6	23
\.


--
-- Name: cafe_productsorder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_productsorder_id_seq', 8, true);


--
-- Data for Name: cafe_table; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_table (id, name, status) FROM stdin;
2	Стол 1	0
3	Стол 2	0
4	Стол 3	0
5	Стол 4	0
6	Стол 5	0
7	Стол 6	0
8	Стол 7	0
9	Стол 8	0
10	Стол 9	0
\.


--
-- Name: cafe_table_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_table_id_seq', 10, true);


--
-- Data for Name: corsheaders_corsmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY corsheaders_corsmodel (id, cors) FROM stdin;
\.


--
-- Name: corsheaders_corsmodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('corsheaders_corsmodel_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2015-03-25 11:08:27.304377+02	1	StuffUser object	1		7	1
2	2015-03-26 14:26:04.441354+02	2	sasha1	3		4	1
3	2015-03-26 14:26:04.470419+02	3	sasha2	3		4	1
4	2015-03-26 14:38:48.538185+02	1	Table 1	1		11	1
5	2015-03-26 14:38:51.098592+02	2	Table 2	1		11	1
6	2015-03-26 14:38:56.606685+02	3	Table 3	1		11	1
7	2015-03-26 14:39:04.033163+02	4	Table 4	1		11	1
8	2015-03-26 14:39:07.578667+02	5	Table 5	1		11	1
9	2015-03-26 14:39:10.852419+02	6	Table 6	1		11	1
10	2015-03-26 14:39:14.677547+02	7	Table 7	1		11	1
11	2015-03-26 14:39:18.587892+02	8	Table 8	1		11	1
12	2015-03-26 14:39:22.238608+02	9	Table 9	1		11	1
13	2015-03-26 14:43:43.454191+02	1	Брускетта с овощами и пармезаном	1		12	1
14	2015-03-26 14:44:06.251817+02	2	Брускетта Маргарита	1		12	1
15	2015-03-26 14:44:38.612441+02	3	Котлетки из судака в сметане с картофельным пюре	1		12	1
16	2015-03-26 14:44:55.469551+02	4	Лосось на помидорини со шпинатом	1		12	1
17	2015-03-26 14:45:27.744037+02	5	Профитроли с кремом Шантили	1		12	1
18	2015-03-26 14:45:56.382248+02	6	Малиновый суп с мороженным	1		12	1
19	2015-03-26 14:46:25.097014+02	7	Мороженое и сорбет в ассортименте	1		12	1
20	2015-03-26 14:46:47.680874+02	8	Cenobio Primitivo Cantele IGT 2011	1		12	1
21	2015-03-26 14:47:05.68858+02	9	Rosso di Montalcino Col d’Orcia DOC 2010	1		12	1
22	2015-03-26 16:50:13.038498+02	1	Order object	2	Added products order "ProductsOrder object". Added products order "ProductsOrder object".	13	1
23	2015-04-01 14:47:28.800233+03	4	Table 2	1		13	1
24	2015-04-01 15:41:27.859026+03	2	Product object	2	Changed name.	12	1
25	2015-04-01 15:43:02.136044+03	3	Product object	2	No fields changed.	12	1
26	2015-04-01 15:43:44.604743+03	4	Product object	2	No fields changed.	12	1
27	2015-04-01 15:44:24.184109+03	5	Product object	2	No fields changed.	12	1
28	2015-04-01 15:44:56.616177+03	6	Product object	2	No fields changed.	12	1
29	2015-04-01 15:45:11.4554+03	7	Product object	2	No fields changed.	12	1
30	2015-04-01 15:45:33.280114+03	8	Product object	2	No fields changed.	12	1
31	2015-04-01 15:45:50.407991+03	9	Product object	2	No fields changed.	12	1
32	2015-04-01 15:46:25.526793+03	1	Product object	2	No fields changed.	12	1
33	2015-04-01 15:47:47.271647+03	2	Product object	2	No fields changed.	12	1
34	2015-04-03 10:45:21.251092+03	1	CategoryMenu object	1		15	1
35	2015-04-03 10:46:45.670467+03	2	Блюда для компании	1		15	1
36	2015-04-03 10:46:52.191861+03	2	Блюда для компании	2	Changed image.	15	1
37	2015-04-03 10:56:31.982016+03	2	Блюда для компании	2	Changed image.	15	1
38	2015-04-03 10:56:37.995933+03	2	Блюда для компании	2	Changed image.	15	1
39	2015-04-03 11:28:08.60739+03	1	Кухня	1		15	1
40	2015-04-03 11:28:16.291631+03	2	Блюда для компании	1		15	1
41	2015-04-03 11:35:47.038757+03	2	Блюда для компании	2	No fields changed.	15	1
42	2015-04-03 11:35:49.077899+03	2	Блюда для компании	2	No fields changed.	15	1
43	2015-04-03 12:05:17.390512+03	9	Rosso di Montalcino Col d’Orcia DOC 2010	3		12	1
44	2015-04-03 12:05:17.763694+03	8	Cenobio Primitivo Cantele IGT 2011	3		12	1
45	2015-04-03 12:05:17.803578+03	7	Мороженое и сорбет в ассортименте	3		12	1
46	2015-04-03 12:05:17.811832+03	6	Малиновый суп с мороженным	3		12	1
47	2015-04-03 12:05:17.820148+03	5	Профитроли с кремом Шантили	3		12	1
48	2015-04-03 12:05:17.828488+03	4	Лосось на помидорини со шпинатом	3		12	1
49	2015-04-03 12:05:17.83691+03	3	Котлетки из судака в сметане с картофельным пюре	3		12	1
50	2015-04-03 12:05:17.845156+03	2	Бркусетта Маргарита	3		12	1
51	2015-04-03 12:05:17.85345+03	1	Брускетта с овощами и пармезаном	3		12	1
52	2015-04-03 12:09:38.151608+03	2	Блюда для компании	3		15	1
53	2015-04-03 12:09:38.221664+03	1	Кухня	3		15	1
54	2015-04-03 12:35:47.48226+03	1	Кухня	1		15	1
55	2015-04-03 12:36:01.066931+03	2	Блюда для компании	1		15	1
56	2015-04-03 12:37:32.112435+03	3	Блюда с живого огня	1		15	1
57	2015-04-03 12:37:34.872782+03	3	Блюда с живого огня	2	No fields changed.	15	1
58	2015-04-03 12:38:21.425078+03	4	Вареники	1		15	1
59	2015-04-03 12:39:02.003166+03	5	Гарниры	1		15	1
60	2015-04-03 12:43:12.050867+03	6	Бар	1		15	1
61	2015-04-03 12:43:27.937273+03	6	Бар	2	No fields changed.	15	1
62	2015-04-03 12:44:10.828948+03	7	Бренди	1		15	1
63	2015-04-03 12:45:05.465644+03	8	Вина Грузии	1		15	1
64	2015-04-03 12:45:50.028667+03	9	Вина Инкерман	1		15	1
65	2015-04-03 12:47:00.701168+03	10	Вина Франции	1		15	1
66	2015-04-03 12:47:40.221225+03	11	Суши меню	1		15	1
67	2015-04-03 12:48:21.248618+03	12	Горячие блюда	1		15	1
68	2015-04-03 12:49:01.664114+03	13	Горячие роллы	1		15	1
69	2015-04-03 12:49:44.588288+03	14	Детской меню	1		15	1
70	2015-04-03 12:50:02.205591+03	15	Горячие блюда	1		15	1
71	2015-04-03 12:50:24.30405+03	16	Канапе	1		15	1
72	2015-04-03 12:50:35.660153+03	17	Напитки	1		15	1
73	2015-04-03 12:51:11.916042+03	4	sauron07	1		4	1
74	2015-04-03 12:51:19.558141+03	4	sauron07	2	Changed is_staff and is_superuser.	4	1
75	2015-04-03 12:54:05.343066+03	4	sauron07	2	Changed first_name, last_name and email.	4	1
76	2015-04-03 12:54:25.249036+03	4	StuffUser object	1		7	1
77	2015-04-03 12:56:36.182604+03	2	Стол 1	1		11	1
78	2015-04-03 12:56:44.759582+03	3	Стол 2	1		11	1
79	2015-04-03 12:56:46.57954+03	4	Стол 3	1		11	1
80	2015-04-03 12:56:51.759903+03	5	Стол 4	1		11	1
81	2015-04-03 12:56:55.109206+03	6	Стол 5	1		11	1
82	2015-04-03 12:56:58.582926+03	7	Стол 6	1		11	1
83	2015-04-03 12:57:02.675127+03	8	Стол 7	1		11	1
84	2015-04-03 12:57:06.504985+03	9	Стол 8	1		11	1
85	2015-04-03 12:57:09.948539+03	10	Стол 9	1		11	1
86	2015-04-03 13:42:04.657129+03	5	admin	1		4	1
87	2015-04-03 13:42:16.912384+03	5	StuffUser object	1		7	1
88	2015-04-03 13:42:45.441553+03	6	manager	1		4	1
89	2015-04-03 13:43:04.92836+03	6	StuffUser object	1		7	1
90	2015-04-03 13:43:44.145597+03	6	manager	2	Changed first_name and last_name.	4	1
91	2015-04-03 13:43:56.567723+03	5	admin	2	Changed is_staff and is_superuser.	4	1
92	2015-04-03 13:44:19.857879+03	7	worker	1		4	1
93	2015-04-03 13:44:36.960325+03	7	worker	2	Changed first_name and last_name.	4	1
94	2015-04-03 13:44:51.097874+03	7	StuffUser object	1		7	1
95	2015-04-03 13:49:21.139692+03	1	Сытная закуска	1		12	1
96	2015-04-03 13:49:48.421755+03	2	Ассорти Япония	1		12	1
97	2015-04-03 13:50:16.669204+03	3	Ассорти из мяса на гриле	1		12	1
98	2015-04-03 13:50:18.324405+03	3	Ассорти из мяса на гриле	2	No fields changed.	12	1
99	2015-04-03 13:50:40.713947+03	4	Ассорти из рыбы и морепродуктов	1		12	1
100	2015-04-03 13:51:01.247555+03	5	Бараньи ребра	1		12	1
101	2015-04-03 13:51:14.444566+03	6	Кефаль филе	1		12	1
102	2015-04-03 13:51:26.674401+03	7	Колбаски ассорти	1		12	1
103	2015-04-03 13:51:38.371732+03	8	Куриное филе в беконе	1		12	1
104	2015-04-03 13:51:49.892082+03	9	Куриные крылышки	1		12	1
105	2015-04-03 13:52:12.585419+03	10	Вареники с капустой	1		12	1
106	2015-04-03 13:52:24.566747+03	11	Вареники с картофелем и грибами	1		12	1
107	2015-04-03 13:52:38.569773+03	12	Вареники с мясом	1		12	1
108	2015-04-03 14:47:52.402205+03	13	Драники из картофеля со сметаной	1		12	1
109	2015-04-03 14:48:06.95503+03	14	Картофель жареный по-домашнему на сале	1		12	1
110	2015-04-03 14:48:20.549946+03	15	Картофель жареный с грибами	1		12	1
111	2015-04-03 14:48:39.457426+03	16	Картофель запеченный в кожуре с травами	1		12	1
112	2015-04-03 14:49:17.161932+03	17	Ахтамар	1		12	1
113	2015-04-03 14:49:28.523781+03	18	Старый Кахети 3 зв.	1		12	1
114	2015-04-03 14:49:41.721633+03	19	Старый Кахети 5 зв.	1		12	1
115	2015-04-03 14:49:53.891413+03	20	Старый Кахети 7 зв.	1		12	1
116	2015-04-03 15:25:28.494784+03	21	Алазанская долина бел. п/сл.	1		12	1
117	2015-04-03 15:25:50.145011+03	22	Алазанская долина кр. п/сл.	1		12	1
118	2015-04-03 15:26:04.488733+03	23	Мукузани кр. сух.	1		12	1
119	2015-04-03 15:26:21.493099+03	24	Инкерман бел. п/сухое	1		12	1
120	2015-04-03 15:26:33.132722+03	25	Инкерман кр. п/сухое	1		12	1
121	2015-04-03 15:26:44.563731+03	26	Саблете Сотерн бел. сладкое	1		12	1
122	2015-04-03 15:27:13.523507+03	27	Лапша Удон	1		12	1
123	2015-04-03 15:27:32.511393+03	27	Лапша Удон	2	Changed description.	12	1
124	2015-04-03 15:27:52.319118+03	28	Лапша Удон	1		12	1
125	2015-04-03 15:28:09.313638+03	29	Темпура кальмары	1		12	1
126	2015-04-03 15:28:28.132223+03	30	Хибачи рис	1		12	1
127	2015-04-03 15:28:47.926673+03	31	Мега креветка	1		12	1
128	2015-04-03 15:29:05.071822+03	32	Осака	1		12	1
129	2015-04-03 15:29:23.592335+03	33	Темпура-ролл	1		12	1
130	2015-04-03 15:29:53.963839+03	34	Куриная котлета "Курочка ряба"	1		12	1
131	2015-04-03 15:30:07.429367+03	35	Куриные кораблики	1		12	1
132	2015-04-03 15:31:44.653943+03	36	Куриные нагетсы с кетчупом	1		12	1
133	2015-04-03 15:31:59.525217+03	37	Канапе с бужениной	1		12	1
134	2015-04-03 15:32:14.816063+03	38	Канапе с ветчиной	1		12	1
135	2015-04-03 15:32:24.217643+03	39	Канапе с копченой куриной грудкой	1		12	1
136	2015-04-03 15:32:37.702985+03	40	Канапе с семгой	1		12	1
137	2015-04-03 15:32:54.093204+03	41	Клубничный коктейль	1		12	1
138	2015-04-03 15:33:04.262718+03	42	Коктейль ананасово-молочный	1		12	1
139	2015-04-03 15:33:13.853547+03	43	 Коктейль шоколадно-банановый	1		12	1
140	2015-04-03 15:33:23.643874+03	44	Молочный коктейль "Тропик"	1		12	1
141	2015-04-03 15:33:37.553727+03	45	Соки в ассортименте	1		12	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 141, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	accounts	stuffuser
8	menu	categorymenu
9	authtoken	token
10	corsheaders	corsmodel
11	cafe	table
12	cafe	product
13	cafe	order
14	cafe	productsorder
15	cafe	categorymenu
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 15, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2015-03-25 10:58:10.910937+02
2	auth	0001_initial	2015-03-25 10:58:11.619206+02
3	accounts	0001_initial	2015-03-25 10:58:11.712453+02
4	admin	0001_initial	2015-03-25 10:58:11.886103+02
5	authtoken	0001_initial	2015-03-25 10:58:12.032585+02
7	sessions	0001_initial	2015-03-25 10:58:12.321036+02
11	accounts	0002_auto_20150326_1011	2015-03-26 12:12:04.093978+02
16	contenttypes	0002_remove_content_type_name	2015-04-03 09:52:41.514738+03
17	auth	0002_alter_permission_name_max_length	2015-04-03 09:52:41.605287+03
18	auth	0003_alter_user_email_max_length	2015-04-03 09:52:41.64704+03
19	auth	0004_alter_user_username_opts	2015-04-03 09:52:41.675494+03
20	auth	0005_alter_user_last_login_null	2015-04-03 09:52:41.705281+03
21	auth	0006_require_contenttypes_0002	2015-04-03 09:52:41.713486+03
28	cafe	0001_initial	2015-04-03 12:20:47.473105+03
29	cafe	0002_product_image	2015-04-03 12:58:51.830129+03
30	cafe	0003_product_description	2015-04-03 13:48:14.833252+03
31	cafe	0004_auto_20150407_1401	2015-04-07 17:02:08.25112+03
33	cafe	0005_auto_20150407_1416	2015-04-07 17:16:40.203283+03
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 33, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
4cc4mjnczxcyfff9ypnhm3j8zylx9g3j	MDljNTYxM2RkYWI5OTAxYWRkMzU4YTUwNmUzYjRhNmRkMzdlOWEzYjp7Il9hdXRoX3VzZXJfaGFzaCI6ImU4MmEwYWJlYjk4NTA3YTViZWVmYzUyZGVkMjUxNGNjY2ZjZjM5OGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-04-09 10:55:21.099184+03
uxr43q4d1p8qj0tee4tsf4yw7pjmyk2j	MDljNTYxM2RkYWI5OTAxYWRkMzU4YTUwNmUzYjRhNmRkMzdlOWEzYjp7Il9hdXRoX3VzZXJfaGFzaCI6ImU4MmEwYWJlYjk4NTA3YTViZWVmYzUyZGVkMjUxNGNjY2ZjZjM5OGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-04-09 17:21:36.477118+03
vqernbsgpew8hvps559xjih47duq0t7o	YjRlMWNkNWEyOTliNDQzMWMzOTg1MzdkNjI5MmM3OGEwZmUzZjI2Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImRjMDFmNzNmYWUyYjliZDE0YjhiOGU4MzQ5Mjk3M2Y4YjViZTk4MTciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2015-04-17 10:36:56.69704+03
d601w97tnvbv95q7tlarhfk2kebjugn2	YjRlMWNkNWEyOTliNDQzMWMzOTg1MzdkNjI5MmM3OGEwZmUzZjI2Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImRjMDFmNzNmYWUyYjliZDE0YjhiOGU4MzQ5Mjk3M2Y4YjViZTk4MTciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2015-04-17 14:46:13.836665+03
w8f2cfl9niu10kfbpcnaowotheef1s09	YjRlMWNkNWEyOTliNDQzMWMzOTg1MzdkNjI5MmM3OGEwZmUzZjI2Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImRjMDFmNzNmYWUyYjliZDE0YjhiOGU4MzQ5Mjk3M2Y4YjViZTk4MTciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2015-04-17 15:25:13.780404+03
vqnfmdzzesc1cpoddvliohd7usnqthih	YjRlMWNkNWEyOTliNDQzMWMzOTg1MzdkNjI5MmM3OGEwZmUzZjI2Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImRjMDFmNzNmYWUyYjliZDE0YjhiOGU4MzQ5Mjk3M2Y4YjViZTk4MTciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2015-04-21 14:41:20.06055+03
\.


--
-- Name: accounts_stuffuser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accounts_stuffuser
    ADD CONSTRAINT accounts_stuffuser_pkey PRIMARY KEY (user_id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: cafe_categorymenu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_categorymenu
    ADD CONSTRAINT cafe_categorymenu_pkey PRIMARY KEY (id);


--
-- Name: cafe_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_order
    ADD CONSTRAINT cafe_order_pkey PRIMARY KEY (id);


--
-- Name: cafe_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_product
    ADD CONSTRAINT cafe_product_pkey PRIMARY KEY (id);


--
-- Name: cafe_productsorder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_productsorder
    ADD CONSTRAINT cafe_productsorder_pkey PRIMARY KEY (id);


--
-- Name: cafe_table_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_table
    ADD CONSTRAINT cafe_table_pkey PRIMARY KEY (id);


--
-- Name: corsheaders_corsmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY corsheaders_corsmodel
    ADD CONSTRAINT corsheaders_corsmodel_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_3ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_3ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: cafe_categorymenu_6be37982; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_categorymenu_6be37982 ON cafe_categorymenu USING btree (parent_id);


--
-- Name: cafe_order_a15b1ede; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_order_a15b1ede ON cafe_order USING btree (table_id);


--
-- Name: cafe_order_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_order_e8701ad4 ON cafe_order USING btree (user_id);


--
-- Name: cafe_product_b583a629; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_product_b583a629 ON cafe_product USING btree (category_id);


--
-- Name: cafe_productsorder_69dfcb07; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_productsorder_69dfcb07 ON cafe_productsorder USING btree (order_id);


--
-- Name: cafe_productsorder_9bea82de; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_productsorder_9bea82de ON cafe_productsorder USING btree (product_id);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: accounts_stuffuser_user_id_6f9260a1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accounts_stuffuser
    ADD CONSTRAINT accounts_stuffuser_user_id_6f9260a1_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permiss_permission_id_23962d04_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_23962d04_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_58c48ba9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_58c48ba9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_51277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_51277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_30a071c9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_30a071c9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_24702650_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_24702650_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_per_permission_id_3d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_3d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_7cd7acb6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_7cd7acb6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token_user_id_535fb363_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_535fb363_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_categorymenu_parent_id_58d5e446_fk_cafe_categorymenu_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_categorymenu
    ADD CONSTRAINT cafe_categorymenu_parent_id_58d5e446_fk_cafe_categorymenu_id FOREIGN KEY (parent_id) REFERENCES cafe_categorymenu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_order_table_id_378e4fb7_fk_cafe_table_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_order
    ADD CONSTRAINT cafe_order_table_id_378e4fb7_fk_cafe_table_id FOREIGN KEY (table_id) REFERENCES cafe_table(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_order_user_id_757a3839_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_order
    ADD CONSTRAINT cafe_order_user_id_757a3839_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_product_category_id_44c01773_fk_cafe_categorymenu_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_product
    ADD CONSTRAINT cafe_product_category_id_44c01773_fk_cafe_categorymenu_id FOREIGN KEY (category_id) REFERENCES cafe_categorymenu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_productsorder_order_id_e8e2aa9_fk_cafe_order_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_productsorder
    ADD CONSTRAINT cafe_productsorder_order_id_e8e2aa9_fk_cafe_order_id FOREIGN KEY (order_id) REFERENCES cafe_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_productsorder_product_id_e952a38_fk_cafe_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_productsorder
    ADD CONSTRAINT cafe_productsorder_product_id_e952a38_fk_cafe_product_id FOREIGN KEY (product_id) REFERENCES cafe_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_5151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_5151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_1c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_1c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

