from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.authtoken import views
from accounts.views import LoginView, StuffUserList, StuffUserDetail, UserList, UserDetail
from cafe.views import TableList, TableDetail, ProductList, ProductDetail, OrdertList, OrderDetail, CategoryMenutList, \
    CategoryMenuDetail
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/api-token-auth/', views.obtain_auth_token),

    url(r'^api/register/$', 'accounts.views.register'),
    url(r'^api/login/$', LoginView.as_view()),
    url(r'^api/logout/$', 'accounts.views.logout'),


    url(r'^api/users/$', UserList.as_view()),
    url(r'^api/users/(?P<pk>[0-9]+)/$', UserDetail.as_view()),
    url(r'^api/stuff-users/$', StuffUserList.as_view()),
    url(r'^api/stuff-users/(?P<pk>[0-9]+)$', StuffUserDetail.as_view()),

    url(r'^api/tables/$', TableList.as_view()),
    url(r'^api/tables/(?P<pk>[0-9]+)$', TableDetail.as_view()),
    url(r'^api/products/$', ProductList.as_view()),
    url(r'^api/products/(?P<pk>[0-9]+)$', ProductDetail.as_view()),
    url(r'^api/orders/$', OrdertList.as_view()),
    url(r'^api/orders/(?P<pk>[0-9]+)$', OrderDetail.as_view()),
    url(r'^api/category-menu/$', CategoryMenutList.as_view()),
    url(r'^api/category-menu/(?P<pk>[0-9]+)$', CategoryMenuDetail.as_view()),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
